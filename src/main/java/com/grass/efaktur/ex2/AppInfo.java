package com.grass.efaktur.ex2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppInfo {
    private static AppInfo INSTANCE;

    private Properties props;

    private AppInfo() {
        this.props = new Properties();
        try (InputStream is = getClass().getResourceAsStream("/appinfo.properties")){
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Properties buildProps = new Properties();
        try (InputStream is = getClass().getResourceAsStream("/appinfo_build.properties")){
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.props.putAll(buildProps);
    }

    public String getProperty(String name) {
        return this.props.getProperty(name);
    }

    public Object getOrDefault(String name, Object def) {
        return this.props.getOrDefault(name, def);
    }

    public static AppInfo getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppInfo();
        }
        return INSTANCE;
    }
}
