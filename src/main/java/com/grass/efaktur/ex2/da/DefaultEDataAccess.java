/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2.da;

/**
 *
 * @author agustian
 */
import com.grass.efaktur.ex2.Configuration;
import com.grass.efaktur.ex2.EData;
import com.grass.efaktur.ex2.EDataLine;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DefaultEDataAccess implements EDataAccess {
  private final Configuration conf;
  
  public DefaultEDataAccess(Configuration conf) {
    this.conf = conf;
  }
  
  @Override
  public EData load(String invoiceNumber) throws EDataAccessException {
    EData result = null;
    try {
      Class.forName("org.hsqldb.jdbcDriver");
    } catch (ClassNotFoundException ex) {
      throw new EDataAccessException("Failed to load driver Class", ex);
    } 
    try {
      String url = "jdbc:hsqldb:hsql://" + this.conf.getDbServerHost() + ":" + this.conf.getDbServerPort().toString() + "/" + this.conf.getDbName();
      Connection con = DriverManager.getConnection(url);
      String query =
              "SELECT\n" +
              "  i.number,\n" +
              "  i.billingdate,\n" +
              "  t.name,\n" +
              "  t.number,\n" +
              "  t.address,\n" +
              "  ts.description,\n" +
              "  ts.percent,\n" +
              "  p.code,\n" +
              "  p.name,\n" +
              "  di.unitprice,\n" +
              "  di.acceptqty\n" +
              "FROM\n" +
              "  \"PUBLIC\".INV i\n" +
              "  LEFT JOIN \"PUBLIC\".CUSTOMER c ON i.CUSTOMER_CODE=c.CODE\n" +
              "  LEFT JOIN \"PUBLIC\".TAXINFO t ON c.TAXINFO_ID=t.id\n" +
              "  LEFT JOIN \"PUBLIC\".INV_TAX_SCHEME its ON i.ID=its.INV_ID\n" +
              "  LEFT JOIN \"PUBLIC\".TAX_SCHEME ts ON its.TAXSCHEMES_ID=ts.ID\n" +
              "  LEFT JOIN \"PUBLIC\".INVITEM ii ON i.ID=ii.INVOICE_ID\n" +
              "  LEFT JOIN \"PUBLIC\".DELORDER d ON ii.DELIVERYORDER_ID=d.ID\n" +
              "  LEFT JOIN \"PUBLIC\".DOITEM di on d.ID=di.DELIVERYORDER_ID\n" +
              "  LEFT JOIN \"PUBLIC\".CO_ITEM ci ON di.CUSTOMERORDERITEM_ID=ci.ID\n" +
              "  LEFT JOIN \"PUBLIC\".ITEM p ON ci.ITEM_CODE=p.CODE\n" +
              "WHERE\n" +
              "  i.NUMBER = ?;";
      PreparedStatement ps = con.prepareStatement(query);
      ps.setString(1, invoiceNumber);
      ResultSet rs = ps.executeQuery();
      result = null;
      while (rs.next()) {
        if (result == null) {
          result = new EData();
          result.setLines(new ArrayList());
          result.setTransactionDate(rs.getDate(2));
          result.setPartnerName(rs.getString(3));
          result.setPartnerNPWP(rs.getString(4));
          result.setPartnerAddress(rs.getString(5));
          result.setTaxDescription(rs.getString(6));
          result.setTaxPercentage(rs.getBigDecimal(7));
        }
        EDataLine line = new EDataLine();
        line.setItemCode(rs.getString(8));
        line.setItemName(rs.getString(9));
        line.setUnitPrice(rs.getBigDecimal(10));
        line.setQuantity(rs.getBigDecimal(11));
        line.setTaxPercentage(rs.getBigDecimal(7));
        if (line.getQuantity().compareTo(BigDecimal.ZERO) == 1)
          result.getLines().add(line); 
      } 
      con.close();
    } catch (SQLException ex) {
      throw new EDataAccessException("Got an SQL exception", ex);
    } 
    return result;
  }
}
