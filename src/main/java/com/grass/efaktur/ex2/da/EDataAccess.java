/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2.da;

/**
 *
 * @author agustian
 */
import com.grass.efaktur.ex2.EData;

public interface EDataAccess {
  EData load(String paramString) throws EDataAccessException;
}

