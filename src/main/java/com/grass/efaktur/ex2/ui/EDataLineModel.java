/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2.ui;

/**
 *
 * @author agustian
 */
import com.grass.efaktur.ex2.EDataLine;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class EDataLineModel extends DefaultTableModel {
  ArrayList<EDataLine> lines;
  
  public EDataLineModel() {
    super((Object[])new String[] { "No.", "Kode", "Nama", "Harga Satuan", "Quantity", "DPP", "PPN" }, 0);
    this.lines = new ArrayList<>();
  }
  
  public void setLines(List<EDataLine> lines) {
    int oldSize = this.lines.size();
    this.lines.clear();
    if (oldSize > 0)
      fireTableRowsDeleted(0, oldSize - 1); 
    this.lines.addAll(lines);
    if (this.lines.size() > 0)
      fireTableRowsInserted(0, this.lines.size() - 1); 
  }
  
  public int getRowCount() {
    return (this.lines != null) ? this.lines.size() : 0;
  }
  
  public int getColumnCount() {
    return 7;
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    EDataLine line = this.lines.get(rowIndex);
    switch (columnIndex) {
      case 0:
        return Integer.valueOf(rowIndex + 1);
      case 1:
        return line.getItemCode();
      case 2:
        return line.getItemName();
      case 3:
        return line.getUnitPrice();
      case 4:
        return line.getQuantity();
      case 5:
        return line.getAmount().setScale(2, RoundingMode.HALF_UP);
      case 6:
        return line.getAmountPPN().setScale(2, RoundingMode.HALF_UP);
    } 
    return null;
  }
}

