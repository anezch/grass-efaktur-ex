/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2.ui;

import com.grass.efaktur.ex2.AppInfo;
import com.grass.efaktur.ex2.Configuration;
import com.grass.efaktur.ex2.EData;
import com.grass.efaktur.ex2.EDataLine;
import com.grass.efaktur.ex2.da.DefaultEDataAccess;
import com.grass.efaktur.ex2.da.EDataAccess;
import com.grass.efaktur.ex2.da.EDataAccessException;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author agustian
 */
public class FrmMain3 extends JFrame {
    
    public FrmMain3 () {
        this.config = new Configuration();
        this.data = new EData();
        this.lastExportPath = null;
        initComponents();

        setTitle(String.format("%s (%s -- %s)",
                AppInfo.getInstance().getProperty("app.name"),
                AppInfo.getInstance().getProperty("app.version"),
                AppInfo.getInstance().getProperty("app.gitHash")));
    }
    
    private void initComponents() {
        MigLayout layout = new MigLayout(
        "fill",
        "[][fill][]",
        "[growprio 0][fill,growprio 100][growprio 0][growprio 0]");
        getContentPane().setLayout(layout);
        setSize(1024, 800);

        DefaultFormatterFactory numberFormatterFactory = new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("#,##0.##")));
        DefaultFormatterFactory dateFormatterFactory = new DefaultFormatterFactory(new DateFormatter(new SimpleDateFormat("yyyy-MM-dd")));

        pnlInputInvoice = new JPanel();
        pnlInputInvoice.setLayout(new MigLayout("fillx", "[growprio 0][growprio 100,fill][growprio 0][growprio 0]"));
        lblInvoiceNumber = new JLabel("Nomor Invoice");
        pnlInputInvoice.add(lblInvoiceNumber);
        txtInvoiceNumber = new JTextField();
        pnlInputInvoice.add(txtInvoiceNumber, "growx");
        btnOpenInvoice = new JButton("Buka");
        btnOpenInvoice.addActionListener(FrmMain3.this::openInvoiceActionPerformed);
        pnlInputInvoice.add(btnOpenInvoice);
        btnExportEFaktur = new JButton("Export");
        btnExportEFaktur.addActionListener(FrmMain3.this::btnExportActionPerformed);
        pnlInputInvoice.add(btnExportEFaktur, "wrap");
        lblStatus = new JLabel("Ready");
        pnlInputInvoice.add(lblStatus, "cell 1 1 3 1, growx");
        getContentPane().add(pnlInputInvoice, "span, growx, wrap");

        splInvoiceData = new JSplitPane();
        getContentPane().add(splInvoiceData, "span, grow, wrap");

        pnlInvoiceSummary = new JPanel();
        pnlInvoiceSummary.setLayout(new MigLayout(
                "fill",
                "[growprio 0, al right][]push"
        ));
        lblPartner = new JLabel("Partner:");
        pnlInvoiceSummary.add(lblPartner);
        txtPartner = new JTextField();
        pnlInvoiceSummary.add(txtPartner, "wmin 300, wrap");
        lblTransactionDate = new JLabel("Tanggal Transaksi:");
        txtTransactionDate = new JFormattedTextField();
        txtTransactionDate.setFormatterFactory(dateFormatterFactory);
        pnlInvoiceSummary.add(lblTransactionDate);
        pnlInvoiceSummary.add(txtTransactionDate, "wmin 150, wrap");
        lblVATDate = new JLabel("Tanggal Faktur Pajak:");
        txtVATDate = new JFormattedTextField();
        txtVATDate.setFormatterFactory(dateFormatterFactory);
        pnlInvoiceSummary.add(lblVATDate);
        pnlInvoiceSummary.add(txtVATDate, "wmin 150, wrap");
        lblTransactionCode = new JLabel("Kode Transaksi:");
        cmbTransactionCode = new JComboBox<>();
        cmbTransactionCode.setModel(new DefaultComboBoxModel<>(new String[]{"01 - Kepada Pihak yang Bukan Pemungut PPN", "02 - Kepada Pemungut Bendaharawan", "03 - Kepada Pemungut Selain Bendaharawan", "04 - DPP Nilai Lain", "06 - Penyerahan Lainnya", "07 - Penyerahan yang PPN-nya Tidak Dipungut", "08 - Penyerahan yang PPN-nya Dibebaskan", "09 - Penyerahan Aktiva (Pasal 16D UU PPN)"}));
        cmbTransactionCode.addItemListener(FrmMain3.this::cmbTransactionCodeItemStateChanged);
        pnlInvoiceSummary.add(lblTransactionCode);
        pnlInvoiceSummary.add(cmbTransactionCode, "wmin 300, wrap");
        lblAddNote = new JLabel("Keterangan Tambahan:");
        cmbAddNote = new JComboBox<>();
        cmbAddNote.setModel(new DefaultComboBoxModel<>(new String[] { "1 - Kawasan Bebas", "2 - Tempat Penimbunan Berikat", "3 - Hibah dan Bantuan Luar Negeri", "4 - Avtur", "5 - Lainnya", "6 - Kontraktor Perjanjian Karya Pengusahaan Pertambangan Batubara Generasi I" }));
        cmbAddNote.setEnabled(false);
        pnlInvoiceSummary.add(lblAddNote);
        pnlInvoiceSummary.add(cmbAddNote, "wmin 300, wrap");
        lblRevisionCode = new JLabel("Kode Pengganti:");
        cmbRevisionCode = new JComboBox<>();
        cmbRevisionCode.setModel(new DefaultComboBoxModel<>(new String[] { "0 - Faktur Pajak", "1 - Faktur Pajak Pengganti" }));
        pnlInvoiceSummary.add(lblRevisionCode);
        pnlInvoiceSummary.add(cmbRevisionCode, "wmin 300, wrap");
        lblSerialNumber = new JLabel("Nomor Seri:");
        txtSerialNumber = new JFormattedTextField();
        try {
            this.txtSerialNumber.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("###-##.########")));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        pnlInvoiceSummary.add(lblSerialNumber);
        pnlInvoiceSummary.add(txtSerialNumber, "wmin 200, wrap");
        lblDocumentReference = new JLabel("Nomor Dokumen Pendukung:");
        txtDocumentReference = new JTextField();
        pnlInvoiceSummary.add(lblDocumentReference);
        pnlInvoiceSummary.add(txtDocumentReference, "wmin 250, wrap");
        lblPartnerNPWP = new JLabel("NPWP Partner:");
        txtPartnerNPWP = new JFormattedTextField();
        pnlInvoiceSummary.add(lblPartnerNPWP);
        pnlInvoiceSummary.add(txtPartnerNPWP, "wmin 250, wrap");
        lblPartnerAddress = new JLabel("Alamat Partner:");
        txtPartnerAddress = new JTextArea();
        scrPartnerAddress = new JScrollPane(txtPartnerAddress);
        pnlInvoiceSummary.add(lblPartnerAddress);
        pnlInvoiceSummary.add(scrPartnerAddress, "wmin 300, hmin 100, grow, wrap");
        lblAmountBeforeTax = new JLabel("Jumlah DPP:");
        txtAmountBeforeTax = new JFormattedTextField();
        txtAmountBeforeTax.setFormatterFactory(numberFormatterFactory);
        pnlInvoiceSummary.add(lblAmountBeforeTax);
        pnlInvoiceSummary.add(txtAmountBeforeTax, "wmin 150, wrap");
        lblAmountVAT = new JLabel("Jumlah PPN:");
        txtAmountVAT = new JFormattedTextField();
        txtAmountVAT.setFormatterFactory(numberFormatterFactory);
        pnlInvoiceSummary.add(lblAmountVAT, "pushy, ay top");
        pnlInvoiceSummary.add(txtAmountVAT, "wmin 150, pushy, ay top, wrap");

        // Read only fields
        txtPartner.setEditable(false);
        txtPartnerAddress.setEditable(false);
        txtPartnerNPWP.setEditable(false);
        txtTransactionDate.setEditable(false);
        txtTransactionDate.setEditable(false);
        txtAmountBeforeTax.setEditable(false);
        txtAmountVAT.setEditable(false);

        scrInvoiceItems = new JScrollPane();
        tblInvoiceItems = new JTable();
        dataLineModel = new EDataLineModel();
        tblInvoiceItems.setModel(dataLineModel);
        NumberCellRenderer numberCellRenderer = new NumberCellRenderer();
        tblInvoiceItems.getColumnModel().getColumn(3).setCellRenderer(numberCellRenderer);
        tblInvoiceItems.getColumnModel().getColumn(4).setCellRenderer(numberCellRenderer);
        tblInvoiceItems.getColumnModel().getColumn(5).setCellRenderer(numberCellRenderer);
        tblInvoiceItems.getColumnModel().getColumn(6).setCellRenderer(numberCellRenderer);
        scrInvoiceItems.setViewportView(tblInvoiceItems);

        JScrollPane scrInvoiceSummary = new JScrollPane(pnlInvoiceSummary);

        splInvoiceData.setLeftComponent(scrInvoiceSummary);
        splInvoiceData.setRightComponent(scrInvoiceItems);
        splInvoiceData.setOneTouchExpandable(true);
        splInvoiceData.setResizeWeight(0.5d);

        sepMain = new JSeparator(JSeparator.HORIZONTAL);
        getContentPane().add(sepMain, "span, growx, wrap");

        btnConfiguration = new JButton("Configure");
        btnConfiguration.addActionListener(FrmMain3.this::onButtonConfigurationActionPerformed);
        getContentPane().add(btnConfiguration, "cell 0 3");
        btnExit = new JButton("Exit");
        btnExit.addActionListener(FrmMain3.this::btnExitActionPerformed);
        getContentPane().add(btnExit, "cell 2 3, align right");
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    void showError(String message) {
        JOptionPane.showMessageDialog(this, message, "ERROR", JOptionPane.ERROR_MESSAGE);
    }

    protected EData loadInvoiceData(String invoiceNumber, EDataAccess dataAccess) {
        if (invoiceNumber.trim().isEmpty()) {
            showError("Invoice number cannot be empty");
            return null;
        }

        EData loadedData;
        try {
            loadedData = dataAccess.load(invoiceNumber);
        } catch (EDataAccessException ex) {
            Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
            showError("Error while reading database: " + ex.getMessage());
            return null;
        }
        if (loadedData == null) {
            showError("No data retrieved. Please check that you have entered a correct Invoice Number");
            return null;
        }
        loadedData.recalculateTotals();
        return loadedData;
    }

    boolean openInvoice(String invoiceNumber) {
        EData loadedData = loadInvoiceData(invoiceNumber, new DefaultEDataAccess((this.config)));
        if (loadedData == null) {
            return false;
        }
        this.data = loadedData;
        // Set default Tax Invoice Date (VAT Date) to the date of the transaction
        this.data.setTaxInvoiceDate(this.data.getTransactionDate());
        return true;
    }

    void updateDataDisplay() {
        this.txtPartner.setText(this.data.getPartnerName());
        this.txtPartnerAddress.setText(this.data.getPartnerAddress());
        this.txtPartnerNPWP.setText(this.data.getPartnerNPWP());
        this.txtTransactionDate.setValue(this.data.getTransactionDate());
        this.txtVATDate.setValue(this.data.getTaxInvoiceDate());
        this.txtAmountBeforeTax.setValue(this.data.getDppAmount());
        this.txtAmountVAT.setValue(this.data.getPpnAmount());
        this.dataLineModel.setLines(this.data.getLines());
    }

    void updateData() {
        Objects.requireNonNull(this.txtTransactionDate.getValue(), "Transaction date must not empty");
        Objects.requireNonNull(this.txtVATDate.getValue(), "EFaktur date must not empty");
        Objects.requireNonNull(this.txtSerialNumber.getValue(), "Serial number must not empty");
        Objects.requireNonNull(this.cmbTransactionCode.getSelectedItem(), "Must select transaction code");
        Objects.requireNonNull(this.cmbRevisionCode.getSelectedItem(), "Must select revision code");

        this.data.setTransactionDate((Date)this.txtTransactionDate.getValue());
        this.data.setTaxInvoiceDate((Date)this.txtVATDate.getValue());
        this.data.setTransactionCode(this.cmbTransactionCode.getSelectedItem().toString().substring(0, 2));

        if (this.data.getTransactionCode().equals("07")) {
            Objects.requireNonNull(this.cmbAddNote.getSelectedItem(), "Must select additional description");
            this.data.setAdditionalDescription(this.cmbAddNote.getSelectedItem().toString().substring(0, 1));
        } else {
            this.data.setAdditionalDescription("");
        }
        this.data.setReplacementFlag(this.cmbRevisionCode.getSelectedItem().toString().substring(0, 1));
        this.data.setSerialNumber(this.txtSerialNumber.getValue().toString());
        this.data.setReference(this.txtInvoiceNumber.getText());
        this.data.setDocumentReference(this.txtDocumentReference.getText());
    }

    private void openInvoiceActionPerformed(ActionEvent evt) {
        String invoiceNumber = this.txtInvoiceNumber.getText();
        boolean opened = openInvoice(invoiceNumber);
        if (opened) {
            this.btnExportEFaktur.setEnabled(true);
            updateDataDisplay();
            this.txtTransactionDate.requestFocus();
        }
    }

    private void onButtonConfigurationActionPerformed(ActionEvent e) {
        DlgConfiguration confDlg = new DlgConfiguration(this, this.config);
        confDlg.setVisible(true);
    }

    private void btnExitActionPerformed(ActionEvent evt) {
        dispose();
    }

    private void btnExportActionPerformed(ActionEvent evt) {
        updateData();
        if (this.lastExportPath == null)
            this.lastExportPath = System.getenv("HOME");
        JFileChooser fileChooser = new JFileChooser(this.lastExportPath);
        fileChooser.setDialogType(1);
        fileChooser.showSaveDialog(this);
        File file = fileChooser.getSelectedFile();
        if (file == null)
            return;
        this.lastExportPath = file.getParent();
        try {
            if (this.data.exportToCSV(file)) {
                JOptionPane.showMessageDialog(this, "File exported successfully", "CSV Export", JOptionPane.INFORMATION_MESSAGE);
            } else {
                showError("Failed to export");
            }
        } catch (IOException ex) {
            Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
            showError("Failed to export: " + ex.getMessage());
        }
    }

    private void cmbTransactionCodeItemStateChanged(ItemEvent evt) {
        this.cmbAddNote.setEnabled(false);
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            String sel = evt.getItem().toString();
            if (sel.startsWith("07"))
                this.cmbAddNote.setEnabled(true);
        }
    }

    public static void main(String [] args) {
        try {
          for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
              UIManager.setLookAndFeel(info.getClassName());
              break;
            } 
          } 
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
          Logger.getLogger(FrmMain3.class.getName()).log(Level.SEVERE, null, ex);
        } 
        EventQueue.invokeLater(() -> (new FrmMain3()).setVisible(true));

    }

    private Configuration config;
    private EData data;
    private String lastExportPath;

    private JPanel pnlInputInvoice;
    private JLabel lblInvoiceNumber;
    private JTextField txtInvoiceNumber;
    private JButton btnOpenInvoice;
    private JButton btnExportEFaktur;
    private JLabel lblStatus;
    private JPanel pnlInvoiceSummary;
    private JLabel lblPartner;
    private JTextField txtPartner;
    private JLabel lblTransactionDate;
    private JFormattedTextField txtTransactionDate;
    private JLabel lblVATDate;
    private JFormattedTextField txtVATDate;
    private JLabel lblTransactionCode;
    private JComboBox<String> cmbTransactionCode;
    private JLabel lblAddNote;
    private JComboBox<String> cmbAddNote;
    private JLabel lblRevisionCode;
    private JComboBox<String> cmbRevisionCode;
    private JLabel lblSerialNumber;
    private JFormattedTextField txtSerialNumber;
    private JLabel lblDocumentReference;
    private JTextField txtDocumentReference;
    private JLabel lblPartnerNPWP;
    private JTextField txtPartnerNPWP;
    private JLabel lblPartnerAddress;
    private JScrollPane scrPartnerAddress;
    private JTextArea txtPartnerAddress;
    private JLabel lblAmountBeforeTax;
    private JFormattedTextField txtAmountBeforeTax;
    private JLabel lblAmountVAT;
    private JFormattedTextField txtAmountVAT;
    private JScrollPane scrInvoiceItems;
    private JTable tblInvoiceItems;
    private JSplitPane splInvoiceData;
    private JButton btnConfiguration;
    private JButton btnExit;
    private JSeparator sepMain;
    
    private EDataLineModel dataLineModel;
}
