/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2.ui;

/**
 *
 * @author agustian
 */
import com.grass.efaktur.ex2.Configuration;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class DlgConfiguration extends JDialog {
  private JButton btnOK;
  
  private Configuration conf;
  
  private JLabel jLabel1;
  
  private JLabel jLabel2;
  
  private JLabel jLabel3;
  
  private JPanel jPanel1;
  
  private JTabbedPane jTabbedPane1;
  
  private JSpinner spnPort;
  
  private JTextField txtDBName;
  
  private JTextField txtHost;
  
  public DlgConfiguration(Frame parent, Configuration conf) {
    super(parent, true);
    initComponents();
    this.conf = conf;
    this.txtHost.setText(this.conf.getDbServerHost());
    this.spnPort.setValue(this.conf.getDbServerPort());
    this.txtDBName.setText(this.conf.getDbName());
  }
  
  private void initComponents() {
    setTitle("Configuration");

    this.conf = new Configuration();
    this.jTabbedPane1 = new JTabbedPane();
    this.jPanel1 = new JPanel();
    this.jLabel1 = new JLabel();
    this.jLabel2 = new JLabel();
    this.jLabel3 = new JLabel();
    this.txtDBName = new JTextField();
    this.txtHost = new JTextField();
    this.spnPort = new JSpinner();
    this.btnOK = new JButton();
    setDefaultCloseOperation(2);
    this.jLabel1.setText("Server Host/IP");
    this.jLabel2.setText("Server Port");
    this.jLabel3.setText("Database Name");
    GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
    this.jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jLabel3, -1, 108, 32767).addComponent(this.jLabel1, -1, -1, 32767).addComponent(this.jLabel2, -1, -1, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 29, 32767).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.txtHost).addComponent(this.spnPort, -2, 73, -2).addComponent(this.txtDBName, GroupLayout.Alignment.TRAILING, -1, 219, 32767)).addContainerGap()));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.txtHost, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.spnPort, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.txtDBName, -2, -1, -2)).addContainerGap(45, 32767)));
    this.jTabbedPane1.addTab("Database", null, this.jPanel1, "");
    this.btnOK.setText("OK");
    this.btnOK.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            DlgConfiguration.this.btnOKActionPerformed(evt);
          }
        });
    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jTabbedPane1, GroupLayout.Alignment.TRAILING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap(-1, 32767).addComponent(this.btnOK).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jTabbedPane1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnOK).addContainerGap()));
    pack();
  }
  
  private void btnOKActionPerformed(ActionEvent evt) {
    this.conf.setDbServerHost(this.txtHost.getText());
    this.conf.setDbServerPort((Integer)this.spnPort.getValue());
    this.conf.setDbName(this.txtDBName.getText());
    setVisible(false);
  }
  
  public static void main(String[] args) {
    try {
      for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        } 
      } 
    } catch (ClassNotFoundException ex) {
      Logger.getLogger(DlgConfiguration.class.getName()).log(Level.SEVERE, (String)null, ex);
    } catch (InstantiationException ex) {
      Logger.getLogger(DlgConfiguration.class.getName()).log(Level.SEVERE, (String)null, ex);
    } catch (IllegalAccessException ex) {
      Logger.getLogger(DlgConfiguration.class.getName()).log(Level.SEVERE, (String)null, ex);
    } catch (UnsupportedLookAndFeelException ex) {
      Logger.getLogger(DlgConfiguration.class.getName()).log(Level.SEVERE, (String)null, ex);
    } 
    EventQueue.invokeLater(new Runnable() {
          public void run() {
            DlgConfiguration dialog = new DlgConfiguration(new JFrame(), new Configuration());
            dialog.addWindowListener(new WindowAdapter() {
                  public void windowClosing(WindowEvent e) {
                    System.exit(0);
                  }
                });
            dialog.setVisible(true);
          }
        });
  }
}