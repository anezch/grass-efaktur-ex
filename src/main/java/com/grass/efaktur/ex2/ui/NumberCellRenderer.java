package com.grass.efaktur.ex2.ui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.text.NumberFormat;

public class NumberCellRenderer extends DefaultTableCellRenderer {

    public NumberCellRenderer() {
        setHorizontalAlignment(JLabel.RIGHT);
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object o, boolean b, boolean b1, int i, int i1) {
        if (o instanceof Number) {
            o = NumberFormat.getInstance().format(o);
        }
        return super.getTableCellRendererComponent(jTable, o, b, b1, i, i1);
    }
}
