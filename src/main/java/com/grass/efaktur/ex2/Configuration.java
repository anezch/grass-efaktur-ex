/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class Configuration {
  private String dbServerHost = "localhost";
  
  private Integer dbServerPort = Integer.valueOf(7884);
  
  private String dbName = "grass-paj";
  
  public void readConfiguration(String fileName) throws FileNotFoundException, IOException {
    Properties props = new Properties();
    InputStream is = new FileInputStream(fileName);
    props.load(is);
    for (Map.Entry<Object, Object> entry : props.entrySet()) {
      if (entry.getKey().equals("dbServerHost"))
        this.dbServerHost = entry.getValue().toString(); 
      if (entry.getKey().equals("dbServerPort"))
        this.dbServerPort = (Integer)entry.getValue(); 
      if (entry.getKey().equals("dbName"))
        this.dbName = entry.getValue().toString(); 
    } 
  }
  
  public String getDbServerHost() {
    return this.dbServerHost;
  }
  
  public void setDbServerHost(String dbServerHost) {
    this.dbServerHost = dbServerHost;
  }
  
  public Integer getDbServerPort() {
    return this.dbServerPort;
  }
  
  public void setDbServerPort(Integer dbServerPort) {
    this.dbServerPort = dbServerPort;
  }
  
  public String getDbName() {
    return this.dbName;
  }
  
  public void setDbName(String dbName) {
    this.dbName = dbName;
  }
}
