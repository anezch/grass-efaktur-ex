/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.grass.efaktur.ex2;

import com.grass.efaktur.ex2.ui.FrmMain3;
import org.oxbow.swingbits.dialog.task.TaskDialogs;

import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author agustian
 */
public class Main {
  public static void main(String[] args) {
    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(Thread thread, Throwable throwable) {
        TaskDialogs.showException(throwable);
      }
    });

    try {
      for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        } 
      } 
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
      Logger.getLogger(FrmMain3.class.getName()).log(Level.SEVERE, (String)null, ex);
    } 
    EventQueue.invokeLater(() -> {
        (new FrmMain3()).setVisible(true);
    });
  }   
}
