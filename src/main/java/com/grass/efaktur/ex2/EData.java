/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2;

/**
 *
 * @author agustian
 */
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EData {
  public static final String PROP_TRANSACTIONDATE = "PROP_TRANSACTIONDATE";
  
  public static final String PROP_TAXINVOICEDATE = "PROP_TAXINVOICEDATE";
  
  public static final String PROP_TRANSACTIONCODE = "PROP_TRANSACTIONCODE";
  
  public static final String PROP_REPLACEMENTFLAG = "PROP_REPLACEMENTFLAG";
  
  public static final String PROP_PARTNERNAME = "PROP_PARTNERNAME";
  
  public static final String PROP_PARTNERNPWP = "PROP_PARTNERNPWP";
  
  public static final String PROP_PARTNERADDRESS = "PROP_PARTNERADDRESS";

  public static final String PROP_TAXDESCRIPTION = "PROP_TAXDESCRIPTION";

  public static final String PROP_TAXPERCENTAGE = "PROP_TAXPERCENTAGE";
  
  public static final String PROP_LINES = "PROP_LINES";
  
  public static final String PROP_DPPAMOUNT = "PROP_DPPAMOUNT";
  
  public static final String PROP_PPNAMOUNT = "PROP_PPNAMOUNT";
  
  public static final String PROP_SERIALNUMBER = "PROP_SERIALNUMBER";

  public static final String PROP_DOCUMENTREFERENCE = "PROP_DOCUMENTREFERENCE";
  
  public static final String PROP_REFERENCE = "PROP_REFERENCE";
  
  public static final String PROP_ADDITIONALDESCRIPTION = "PROP_ADDITIONALDESCRIPTION";
  
  private String reference;
  
  private Date transactionDate;
  
  private Date taxInvoiceDate;
  
  private String serialNumber;

  private String documentReference;
  
  private String transactionCode;
  
  private String additionalDescription;
  
  private String replacementFlag;
  
  private String partnerName;
  
  private String partnerNPWP;
  
  private String partnerAddress;

  private String taxDescription;

  private BigDecimal taxPercentage;
  
  private List<EDataLine> lines;
  
  private BigDecimal dppAmount;
  
  private BigDecimal ppnAmount;
  
  private final transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  public EData() {
    this.transactionCode = "01";
    this.replacementFlag = "0";
  }
  
  public List<String> generateCSVLines() {
    List<String> result = new ArrayList<>();
    result.add("\"FK\",\"KD_JENIS_TRANSAKSI\",\"FG_PENGGANTI\",\"NOMOR_FAKTUR\",\"MASA_PAJAK\",\"TAHUN_PAJAK\",\"TANGGAL_FAKTUR\",\"NPWP\",\"NAMA\",\"ALAMAT_LENGKAP\",\"JUMLAH_DPP\",\"JUMLAH_PPN\",\"JUMLAH_PPNBM\",\"ID_KETERANGAN_TAMBAHAN\",\"FG_UANG_MUKA\",\"UANG_MUKA_DPP\",\"UANG_MUKA_PPN\",\"UANG_MUKA_PPNBM\",\"REFERENSI\",\"KODE_DOKUMEN_PENDUKUNG\"");
    result.add("\"LT\",\"NPWP\",\"NAMA\",\"JALAN\",\"BLOK\",\"NOMOR\",\"RT\",\"RW\",\"KECAMATAN\",\"KELURAHAN\",\"KABUPATEN\",\"PROPINSI\",\"KODE_POS\",\"NOMOR_TELEPON\"");
    result.add("\"OF\",\"KODE_OBJEK\",\"NAMA\",\"HARGA_SATUAN\",\"JUMLAH_BARANG\",\"HARGA_TOTAL\",\"DISKON\",\"DPP\",\"PPN\",\"TARIF_PPNBM\",\"PPNBM\",,,,,,,,");
    result.add(String.format("\"FK\",\"%s\",\"%s\",\"%s\",%d,%d,%s,\"%s\",\"%s\",\"%s\",%d,%d,%d,%s,%d,%d,%d,%d,\"%s\",\"%s\"",
            getTransactionCode(), getReplacementFlag(), getSerialNumber().replaceAll("[.-]", ""),
            getDateMonth(getTaxInvoiceDate()), getDateYear(getTaxInvoiceDate()),
            (new SimpleDateFormat("dd/MM/yyyy")).format(getTaxInvoiceDate()),
            getPartnerNPWP().replaceAll("[.-]", ""), getPartnerName(), getPartnerAddress(),
            getDppAmount().intValue(), getPpnAmount().intValue(), 0,
            getTransactionCode().equals("07") ? getAdditionalDescription() : "",
            0, 0, 0, 0, getReference(), getDocumentReference()));
    for (EDataLine line : getLines()) {
      result.add(String.format("\"OF\",\"%s\",\"%s\",%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d,,,,,,,,",
              line.getItemCode(), line.getItemName(), line.getUnitPrice(), line.getQuantity(),
              line.getAmount(), 0.0D, line.getAmount(), line.getAmountPPN(),
              0, 0));
    }
    return result;
  }

  public boolean exportToCSV(File file) throws IOException {
      try (PrintStream ps = new PrintStream(file)) {
        for (String line : generateCSVLines()) {
          ps.println(line);
        }
        ps.flush();
      }
    return true;
  }

  public boolean exportToCSV(String fileName) throws IOException {
    return exportToCSV(new File(fileName));
  }

  int getDateMonth(Date d) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(d);
    return cal.get(2) + 1;
  }
  
  int getDateYear(Date d) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(d);
    return cal.get(1);
  }
  
  public Date getTransactionDate() {
    return this.transactionDate;
  }
  
  public void setTransactionDate(Date transactionDate) {
    Date oldTransactionDate = this.transactionDate;
    this.transactionDate = transactionDate;
    this.propertyChangeSupport.firePropertyChange("PROP_TRANSACTIONDATE", oldTransactionDate, transactionDate);
  }
  
  public Date getTaxInvoiceDate() {
    return this.taxInvoiceDate;
  }
  
  public void setTaxInvoiceDate(Date taxInvoiceDate) {
    Date oldTaxInvoiceDate = this.taxInvoiceDate;
    this.taxInvoiceDate = taxInvoiceDate;
    this.propertyChangeSupport.firePropertyChange("PROP_TAXINVOICEDATE", oldTaxInvoiceDate, taxInvoiceDate);
  }
  
  public String getTransactionCode() {
    return this.transactionCode;
  }
  
  public void setTransactionCode(String transactionCode) {
    String oldTransactionCode = this.transactionCode;
    this.transactionCode = transactionCode;
    this.propertyChangeSupport.firePropertyChange("PROP_TRANSACTIONCODE", oldTransactionCode, transactionCode);
  }
  
  public String getReplacementFlag() {
    return this.replacementFlag;
  }
  
  public void setReplacementFlag(String replacementFlag) {
    String oldReplacementFlag = this.replacementFlag;
    this.replacementFlag = replacementFlag;
    this.propertyChangeSupport.firePropertyChange("PROP_REPLACEMENTFLAG", oldReplacementFlag, replacementFlag);
  }
  
  public String getPartnerName() {
    return this.partnerName;
  }
  
  public void setPartnerName(String partnerName) {
    String oldPartnerName = this.partnerName;
    this.partnerName = partnerName;
    this.propertyChangeSupport.firePropertyChange("PROP_PARTNERNAME", oldPartnerName, partnerName);
  }
  
  public String getPartnerNPWP() {
    return this.partnerNPWP;
  }
  
  public void setPartnerNPWP(String partnerNPWP) {
    String oldPartnerNPWP = this.partnerNPWP;
    this.partnerNPWP = partnerNPWP;
    this.propertyChangeSupport.firePropertyChange("PROP_PARTNERNPWP", oldPartnerNPWP, partnerNPWP);
  }
  
  public String getPartnerAddress() {
    return this.partnerAddress;
  }
  
  public void setPartnerAddress(String partnerAddress) {
    String oldPartnerAddress = this.partnerAddress;
    this.partnerAddress = partnerAddress;
    this.propertyChangeSupport.firePropertyChange("PROP_PARTNERADDRESS", oldPartnerAddress, partnerAddress);
  }

  public String getTaxDescription() {
    return this.taxDescription;
  }

  public void setTaxDescription(String taxDescription) {
    String oldTaxDescription = this.taxDescription;
    this.taxDescription = taxDescription;
    this.propertyChangeSupport.firePropertyChange(PROP_TAXDESCRIPTION, oldTaxDescription, taxDescription);
  }

  public BigDecimal getTaxPercentage() {
    return this.taxPercentage;
  }

  public void setTaxPercentage(BigDecimal taxPercentage) {
    BigDecimal oldTaxPercentage = this.taxPercentage;
    this.taxPercentage = taxPercentage;
    this.propertyChangeSupport.firePropertyChange(PROP_TAXPERCENTAGE, oldTaxPercentage, taxPercentage);
  }
  
  public List<EDataLine> getLines() {
    return this.lines;
  }
  
  public void setLines(List<EDataLine> lines) {
    List<EDataLine> oldLines = this.lines;
    this.lines = lines;
    this.propertyChangeSupport.firePropertyChange("PROP_LINES", oldLines, lines);
  }

  public void recalculateTotals() {
    BigDecimal newDppAmount = BigDecimal.ZERO;
    BigDecimal newPpnAmount = BigDecimal.ZERO;
    for (EDataLine line : this.lines) {
      line.setTaxPercentage(this.getTaxPercentage());
      newDppAmount = newDppAmount.add(line.getAmount());
      newPpnAmount = newPpnAmount.add(line.getAmountPPN());
    }
    setDppAmount(newDppAmount);
    setPpnAmount(newPpnAmount);
  }
  
  public BigDecimal getDppAmount() {
    return this.dppAmount;
  }
  
  public void setDppAmount(BigDecimal dppAmount) {
    BigDecimal oldDppAmount = this.dppAmount;
    this.dppAmount = dppAmount;
    this.propertyChangeSupport.firePropertyChange("PROP_DPPAMOUNT", oldDppAmount, dppAmount);
  }
  
  public BigDecimal getPpnAmount() {
    return this.ppnAmount;
  }
  
  public void setPpnAmount(BigDecimal ppnAmount) {
    BigDecimal oldPpnAmount = this.ppnAmount;
    this.ppnAmount = ppnAmount;
    this.propertyChangeSupport.firePropertyChange("PROP_PPNAMOUNT", oldPpnAmount, ppnAmount);
  }
  
  public String getSerialNumber() {
    return this.serialNumber;
  }
  
  public void setSerialNumber(String serialNumber) {
    String oldSerialNumber = this.serialNumber;
    this.serialNumber = serialNumber;
    this.propertyChangeSupport.firePropertyChange("PROP_SERIALNUMBER", oldSerialNumber, serialNumber);
  }
  
  public String getReference() {
    return this.reference;
  }
  
  public void setReference(String reference) {
    String oldReference = this.reference;
    this.reference = reference;
    this.propertyChangeSupport.firePropertyChange("PROP_REFERENCE", oldReference, reference);
  }

  public String getDocumentReference() {
    return this.documentReference;
  }

  public void setDocumentReference(String documentReference) {
    String oldDocumentReference = this.documentReference;
    this.documentReference = documentReference;
    this.propertyChangeSupport.firePropertyChange(PROP_DOCUMENTREFERENCE, oldDocumentReference, documentReference);
  }
  
  public String getAdditionalDescription() {
    return this.additionalDescription;
  }
  
  public void setAdditionalDescription(String additionalDescription) {
    String oldAdditionalDescription = this.additionalDescription;
    this.additionalDescription = additionalDescription;
    this.propertyChangeSupport.firePropertyChange("PROP_ADDITIONALDESCRIPTION", oldAdditionalDescription, additionalDescription);
  }
}
