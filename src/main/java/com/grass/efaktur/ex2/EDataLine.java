/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.grass.efaktur.ex2;

/**
 *
 * @author agustian
 */
import java.math.BigDecimal;

public class EDataLine {
  private String itemCode;
  
  private String itemName;
  
  private BigDecimal quantity;
  
  private BigDecimal unitPrice;

  private BigDecimal taxPercentage;
  
  public BigDecimal getAmount() {
    if (this.quantity == null || this.unitPrice == null)
      return BigDecimal.ZERO; 
    return this.quantity.multiply(this.unitPrice);
  }
  
  public BigDecimal getAmountPPN() {
    return this.taxPercentage != null
            ? this.taxPercentage.multiply(getAmount()).divide(BigDecimal.valueOf(100))
            : BigDecimal.ZERO;
  }
  
  public String getItemCode() {
    return this.itemCode;
  }
  
  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }
  
  public String getItemName() {
    return this.itemName;
  }
  
  public void setItemName(String itemName) {
    this.itemName = itemName;
  }
  
  public BigDecimal getQuantity() {
    return this.quantity;
  }
  
  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }
  
  public BigDecimal getUnitPrice() {
    return this.unitPrice;
  }
  
  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public BigDecimal getTaxPercentage() {
    return this.taxPercentage;
  }

  public void setTaxPercentage(BigDecimal taxPercentage) {
    this.taxPercentage = taxPercentage;
  }
}
