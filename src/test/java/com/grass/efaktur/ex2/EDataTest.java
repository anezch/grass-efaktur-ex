package com.grass.efaktur.ex2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EDataTest {

    private EData eData;

    @Before
    public void setUp() {
        eData = new EData();
        eData.setTransactionDate(Date.from(LocalDate.of(2022, 4, 9).atStartOfDay().toInstant(ZoneOffset.UTC)));
        eData.setPartnerName("Test Partner");
        eData.setPartnerNPWP("Partner NPWP");
        eData.setPartnerAddress("Partner Address");
        eData.setTaxDescription("Tax Description");
        eData.setTaxPercentage(new BigDecimal(10));
        eData.setReference("Inv Number");

        eData.setSerialNumber("VAT Serial");
        eData.setTaxInvoiceDate(eData.getTransactionDate());
        eData.setTransactionCode("07");
        eData.setReplacementFlag("0");
        eData.setAdditionalDescription("2");
        eData.setDocumentReference("Doc Ref");

        eData.setLines(new ArrayList<>());
        for (int i=0; i<10; i++) {
            EDataLine line = new EDataLine();
            line.setItemCode(String.format("ITM-%02d", i+1));
            line.setItemName(String.format("Item %02d", i+1));
            line.setQuantity(new BigDecimal(i+1));
            line.setUnitPrice(new BigDecimal(1000 + (i*100)));
            eData.getLines().add(line);
        }

        eData.recalculateTotals();
    }

    @Test
    public void testRecalculateTotals() {
        Assert.assertEquals("DPP Total", 88000, eData.getDppAmount().intValue());
        Assert.assertEquals("PPN Total", 8800, eData.getPpnAmount().intValue());

        eData.setTaxPercentage(new BigDecimal(11));
        eData.recalculateTotals();
        Assert.assertEquals("DPP Total 11%", 88000, eData.getDppAmount().intValue());
        Assert.assertEquals("PPN Total 11%", 9680, eData.getPpnAmount().intValue());
    }

    @Test
    public void testExportToCSV() {
        List<String> csvLines = eData.generateCSVLines();
        Assert.assertEquals("Exported CSV number of lines", 14, csvLines.size());
        Assert.assertEquals("First row of CSV",
                "\"FK\",\"KD_JENIS_TRANSAKSI\",\"FG_PENGGANTI\",\"NOMOR_FAKTUR\",\"MASA_PAJAK\",\"TAHUN_PAJAK\",\"TANGGAL_FAKTUR\",\"NPWP\",\"NAMA\",\"ALAMAT_LENGKAP\",\"JUMLAH_DPP\",\"JUMLAH_PPN\",\"JUMLAH_PPNBM\",\"ID_KETERANGAN_TAMBAHAN\",\"FG_UANG_MUKA\",\"UANG_MUKA_DPP\",\"UANG_MUKA_PPN\",\"UANG_MUKA_PPNBM\",\"REFERENSI\",\"KODE_DOKUMEN_PENDUKUNG\"",
                csvLines.get(0));
        Assert.assertEquals("Second row of CSV",
                "\"LT\",\"NPWP\",\"NAMA\",\"JALAN\",\"BLOK\",\"NOMOR\",\"RT\",\"RW\",\"KECAMATAN\",\"KELURAHAN\",\"KABUPATEN\",\"PROPINSI\",\"KODE_POS\",\"NOMOR_TELEPON\"",
                csvLines.get(1));
        Assert.assertEquals("Third row of CSV",
                "\"OF\",\"KODE_OBJEK\",\"NAMA\",\"HARGA_SATUAN\",\"JUMLAH_BARANG\",\"HARGA_TOTAL\",\"DISKON\",\"DPP\",\"PPN\",\"TARIF_PPNBM\",\"PPNBM\",,,,,,,,",
                csvLines.get(2));
        Assert.assertEquals("Fourth row of CSV",
                String.format("\"FK\",\"%s\",\"%s\",\"%s\",%d,%d,%s,\"%s\",\"%s\",\"%s\",%d,%d,%d,%s,%d,%d,%d,%d,\"%s\",\"%s\"",
                        "07", "0", "VAT Serial",
                        4, 2022,
                        "09/04/2022",
                        "Partner NPWP", "Test Partner", "Partner Address",
                        88000, 8800, 0,
                        "2",
                        0, 0, 0, 0, "Inv Number", "Doc Ref"),
                csvLines.get(3));
    }
}
