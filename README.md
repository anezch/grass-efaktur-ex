grass-efaktur-ex2
=================

[![Latest Release](https://gitlab.com/anezch/grass-efaktur-ex/-/badges/release.svg)](https://gitlab.com/anezch/grass-efaktur-ex/-/releases) [![pipeline status](https://gitlab.com/anezch/grass-efaktur-ex/badges/master/pipeline.svg)](https://gitlab.com/anezch/grass-efaktur-ex/-/commits/master)

Export Grass invoice data for EFaktur.

## Requirements

- JDK 1.8

## Building

```sh
./gradlew clean
./gradlew assemble
```

## Development

### IDE

I personally use IntelliJ IDEA but you can use any IDE that supports gradle. You can even use VIM + command line gradle for small edits.

### Release

To mark a new release:

1. Bump version in `gradle.properties` file
2. Tag commit with the same version with: `git tag -a <version>` and write changelog in the tag message.
3. Push with `git push --tags origin <branch>`
4. MR as usual to master.
5. The rest will be handled by gitlab CI.

Alternative:

1. Bump version in `gradle.propeties`
2. Commit & Push
3. MR as usual
4. Create Tag through GitLab UI (Note, as per [this bug](https://gitlab.com/gitlab-org/release-cli/-/issues/73), do not write Release notes in the tag creation form. It will make 'deploy' job in CI/CD fail).

